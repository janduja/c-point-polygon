/* 
 * Copyright 2017 Raffaele Arecchi <raffaele.arecchi@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stddef.h>

/*
 * Return the abscissa projection value of point p into segment with vertext v1 and v2.
 */
double abscissa_projection(
	double p_y,
	double v1_x,
	double v1_y,
	double v2_x,
	double v2_y);

/*
 * Return the quandrant of point p(x2, y2) relative to p(x2, y2), -1 if the are the same.
 * 0 if [0,pi/2)
 * 1 if [pi/2,pi)
 * 2 if [pi,3pi/4)
 * 3 if [3pi/4,2pi)
 */
int quadrant(double x1, double y1, double x2, double y2);

/*
 * Calculate the integer counterclockwise movement in quadrants from last-point to next-point.
 * Return 9 if p is on the border of the segment with last-point and next-point as vertex.
 */
int calculate_increment(
	double p_x,
	double p_y,
	double last_x,
	double last_y,
	double next_x,
	double next_y);

/*
 * Tell if point p is inside or in the border of polygon. The coordinates of the first element
 * and the last element of the polygon must be equal.
 */
int is_inside_or_border(
	double p_x,
	double p_y,
	void **polygon,
	size_t nitems,
	double (*xcoordinate)(void*),
	double (*ycoordinate)(void*)  
);
