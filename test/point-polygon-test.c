/* 
 * Copyright 2017 Raffaele Arecchi <raffaele.arecchi@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <check.h>
#include <glib-object.h>
#include <json-glib/json-glib.h>
#include "../point-polygon.h"

START_TEST(quadrant_test)
{
	double x1 = 34.134134;
	double y1 = 94.341819;
  ck_assert_int_eq(0, quadrant(x1, y1, 35.2482, 94.341819));
  ck_assert_int_eq(0, quadrant(x1, y1, 35.2482, 102.43948));
  ck_assert_int_eq(1, quadrant(x1, y1, 34.134134, 102.43948));
  ck_assert_int_eq(1, quadrant(x1, y1, -24.34938, 102.43948));
  ck_assert_int_eq(2, quadrant(x1, y1, -24.34938, 94.341819));
  ck_assert_int_eq(2, quadrant(x1, y1, -24.34938, 12.349839));
  ck_assert_int_eq(3, quadrant(x1, y1, 34.134134, 12.349839));
  ck_assert_int_eq(3, quadrant(x1, y1, 35.2482, 12.349839));
  ck_assert_int_eq(-1, quadrant(x1, y1, 34.134134, 94.341819));
}
END_TEST

START_TEST(abscissa_projection_test)
{
	ck_assert(1.0 == abscissa_projection(-1.0, 1.0, 1.0, 1.0, -2.0));
	ck_assert(1.0 == abscissa_projection(1.0, 1.0, 1.0, 2.0, 0.0));
	ck_assert(0.0 == abscissa_projection(0.0, 1.0, -1.0, -2.0, 2.0));
	ck_assert(0.0 == abscissa_projection(1.0, 0.0, 1.0, -2.0, 2.0));
	double projection = abscissa_projection(-32.99, 3.0, -52.01, 22.214, 0.42);
	ck_assert(projection > 9.97025);
	ck_assert(projection < 9.97026);
}
END_TEST

START_TEST(calculate_increment_test)
{
	ck_assert_int_eq(0, calculate_increment(1.23, 0.34, 34.2, 0.44, 2.2, 4.11));
	ck_assert_int_eq(0, calculate_increment(1.23, 0.34, 34.2, 0.44, 2.2, 0.34));
	ck_assert_int_eq(1, calculate_increment(1.23, 0.34, 34.2, 0.44, 1.23, 3.99));
	ck_assert_int_eq(1, calculate_increment(1.23, 0.34, 4.2, 2.44, -3.3, 0.99));
	ck_assert_int_eq(1, calculate_increment(-1.23, -0.34, -4.2, -2.44, 3.3, -0.99));
	ck_assert_int_eq(1, calculate_increment(-1.23, -0.34, 3.3, -0.99, 3.3, -0.1));
	ck_assert_int_eq(-1, calculate_increment(-1.23, -0.34, 3.3, -0.1, 3.3, -0.99));
	ck_assert_int_eq(-1, calculate_increment(1.23, 0.34, -3.3, -0.99, 1.23, 2.44));
	ck_assert_int_eq(-1, calculate_increment(1.23, 0.34, 1.23, 20.9, 3.23, 0.34));
	ck_assert_int_eq(-1, calculate_increment(1.23, 0.34, 1.43, 20.9, 1.23, -34.84));
	ck_assert_int_eq(2, calculate_increment(0.0, 0.0, 1.0, 1.1, -1.0, -1.0));
	ck_assert_int_eq(-2, calculate_increment(0.0, 0.0, -1.0, -1.0, 1.0, 1.1));
	ck_assert_int_eq(-2, calculate_increment(0.0, 0.0, 1.0, 0.9, -1.0, -1.0));
	ck_assert_int_eq(2, calculate_increment(0.0, 0.0, -1.0, -1.0, 1.0, 0.9));
	ck_assert_int_eq(-2, calculate_increment(-4.0, 4.0, -6.0, 6.1, -2.0, 2.0));
	ck_assert_int_eq(2, calculate_increment(-4.0, 4.0, -2.0, 2.0, -6.0, 6.1));
	ck_assert_int_eq(2, calculate_increment(-4.0, 4.0, -6.0, 6.0, -2.1, 2.0));
	ck_assert_int_eq(-2, calculate_increment(-4.0, 4.0, -2.1, 2.0, -6.0, 6.0));
	ck_assert_int_eq(2, calculate_increment(-4.0, 4.0, -6.0, 6.0, -2.0, 1.99));
	ck_assert_int_eq(-2, calculate_increment(-4.0, 4.0, -2.0, 1.99, -6.0, 6.0));
	ck_assert_int_eq(2, calculate_increment(-4.0, 4.0, -1.99, 2.0, -6.0, 6.0));
	ck_assert_int_eq(-2, calculate_increment(-4.0, 4.0, -6.0, 6.0, -1.99, 2.0));
	ck_assert_int_eq(9, calculate_increment(-4.0, 4.0, -2.0, 6.0, -6.0, 2.0));
	ck_assert_int_eq(9, calculate_increment(0.0, 0.0, -1.0, -1.0, 0.5, 0.5));
	ck_assert_int_eq(9, calculate_increment(0.0, 0.0, 0.0, 0.0, 0.5, 0.5));
	ck_assert_int_eq(9, calculate_increment(1.3, 4.0, -1.0, -1.0, 1.3, 4.0));
	ck_assert_int_eq(9, calculate_increment(1.3, 4.0, -1.0, 4.0, 2.3, 4.0));
	ck_assert_int_eq(9, calculate_increment(1.3, 4.0, 1.3, 4.2, 1.3, 2.0));
}
END_TEST

struct Coordinate {
	double x;
	double y;
};

static double get_x(void *c){
	struct Coordinate *cc = (struct Coordinate *) c;
	return (*cc).x;
}

static double get_y(void *c){
	struct Coordinate *cc = (struct Coordinate *) c;
	return (*cc).y;
}

START_TEST(is_inside_trapezoid_test)
{
	struct Coordinate trapezoid[5];
	trapezoid[0].x = 23.24;
	trapezoid[0].y = 51.87;
	trapezoid[1].x = 27.29;
	trapezoid[1].y = 53.99;
	trapezoid[2].x = 29.48;
	trapezoid[2].y = 52.14;
	trapezoid[3].x = 25.07;
	trapezoid[3].y = 50.99;
	trapezoid[4].x = 23.24;
	trapezoid[4].y = 51.87;
	void *polygon[5];
	int i;
	for(i = 0; i < 5; i++)
	{
    polygon[i] = &trapezoid[i];
  }
	ck_assert(is_inside_or_border(27.90, 52.76, polygon, 5, get_x, get_y));
	ck_assert(is_inside_or_border(29.48, 52.14, polygon, 5,  get_x, get_y));
	ck_assert(is_inside_or_border(26.9, 51.76, polygon, 5, get_x, get_y));
	ck_assert(is_inside_or_border(24.6, 51.43, polygon, 5, get_x, get_y));
	ck_assert(!is_inside_or_border(22.6, 51.43, polygon, 5, get_x, get_y));
	ck_assert(!is_inside_or_border(22.6, 53.43, polygon, 5, get_x, get_y));
	ck_assert(!is_inside_or_border(28.6, 53.99, polygon, 5, get_x, get_y));
	ck_assert(!is_inside_or_border(28.6, 51.79, polygon, 5, get_x, get_y));
}
END_TEST

START_TEST(is_inside_spiralid_test)
{
	struct Coordinate spiralid[15];
	spiralid[0].x = 29.71;
	spiralid[0].y = 52.96;
	spiralid[1].x = 30.39;
	spiralid[1].y = 52.02;
	spiralid[2].x = 30.33;
	spiralid[2].y = 53.26;
	spiralid[3].x = 29.23;
	spiralid[3].y = 53.24;
	spiralid[4].x = 29.03;
	spiralid[4].y = 52.70;
	spiralid[5].x = 30.88;
	spiralid[5].y = 52.68;
	spiralid[6].x = 31.02;
	spiralid[6].y = 53.40;
	spiralid[7].x = 29.52;
	spiralid[7].y = 53.76;
	spiralid[8].x = 31.33;
	spiralid[8].y = 54.11;
	spiralid[9].x = 31.65;
	spiralid[9].y = 52.28;
	spiralid[10].x = 28.68;
	spiralid[10].y = 52.58;
	spiralid[11].x = 28.88;
	spiralid[11].y = 53.54;
	spiralid[12].x = 30.73;
	spiralid[12].y = 53.33;
	spiralid[13].x = 30.49;
	spiralid[13].y = 52.82;
	spiralid[14].x = 29.71;
	spiralid[14].y = 52.96;
	void *polygon[15];
	int i;
	for(i = 0; i < 15; i++)
	{
    polygon[i] = &spiralid[i];
  }
	ck_assert(!is_inside_or_border(22.90, 53.02, polygon, 15, get_x, get_y));
	ck_assert(!is_inside_or_border(30.90, 53.02, polygon, 15, get_x, get_y));
	ck_assert(!is_inside_or_border(30.90, 52.02, polygon, 15, get_x, get_y));
	ck_assert(!is_inside_or_border(31.40, 54.02, polygon, 15, get_x, get_y));
	ck_assert(is_inside_or_border(30.40, 52.88, polygon, 15, get_x, get_y));
	ck_assert(is_inside_or_border(31.33, 54.11, polygon, 15, get_x, get_y));
	ck_assert(is_inside_or_border(29.00, 53.38, polygon, 15, get_x, get_y));
	ck_assert(is_inside_or_border(29.00, 52.58, polygon, 15, get_x, get_y));
	ck_assert(is_inside_or_border(30.43, 53.88, polygon, 15, get_x, get_y));
}
END_TEST

START_TEST(is_inside_ireland_test)
{
  GError *error;
  error = NULL;
 
  JsonParser *parser = json_parser_new ();
  json_parser_load_from_file (parser, "test/republic-of-ireland-seawater-boundaries-2017-09-03.geojson", &error);
  JsonNode *root = json_parser_get_root (parser);
  JsonArray *coordinates = json_node_get_array(root);
  guint length = json_array_get_length(coordinates);
  
  struct Coordinate ireland_polygon[length];
  int i;
  for(i=0; i<length; i++) {
		JsonNode *coord = json_array_get_element(coordinates, i);
		JsonArray *coordArray = json_node_get_array(coord);
		JsonNode *x = json_array_get_element(coordArray, 0);
		ireland_polygon[i].x = json_node_get_double(x);
		JsonNode *y = json_array_get_element(coordArray, 1);
		ireland_polygon[i].y = json_node_get_double(y);
	}
  json_node_free (root);
  g_object_unref (parser);
  void *polygon[length];
  for(i = 0; i < length; i++)
	{
    polygon[i] = &ireland_polygon[i];
  }
	
	struct Coordinate dublin;
	dublin.x = -6.2576209;
	dublin.y = 53.3293802;
	struct Coordinate belfast;
	belfast.x = -6.0670604;
	belfast.y = 54.5949977;
	struct Coordinate monaghan;
	monaghan.x = -6.96938;
	monaghan.y = 54.24657;
	struct Coordinate manorhamilton;
	manorhamilton.x = -8.17908;
	manorhamilton.y = 54.30580;
	struct Coordinate bordernearmagheraaveli;
	bordernearmagheraaveli.x = -7.23844650000001;
	bordernearmagheraaveli.y = 54.2073289991055;
	struct Coordinate moville;
	moville.x = -7.03672;
	moville.y = 55.18909;
	struct Coordinate backlion;
	backlion.x = -7.87125;
	backlion.y = 54.29091;
	struct Coordinate cork;
	cork.x = -8.4653;
	cork.y = 51.8938;
	struct Coordinate galway;
	galway.x = -9.04726;
	galway.y = 53.27090;	
	struct Coordinate castletownbere;
	castletownbere.x = -9.90941;
	castletownbere.y = 51.64889;
	struct Coordinate armagh;
	armagh.x = -6.65447;
	armagh.y = 54.34985;
	struct Coordinate kilgarrowlaugh;
	kilgarrowlaugh.x = -7.36034;
	kilgarrowlaugh.y = 54.14523;
	struct Coordinate london;
	london.x = -0.0882;
	london.y = 51.4898;
	struct Coordinate derry;
	derry.x = -7.2963;
	derry.y = 54.9943;
	struct Coordinate overatlanticcoast;
	overatlanticcoast.x = -16.337;
	overatlanticcoast.y = 54.889;
	struct Coordinate montreal;
	montreal.x = -73.5040;
	montreal.y = 45.5059;
	struct Coordinate buenosaires;
	buenosaires.x = -57.9749;
	buenosaires.y = -34.6083;
	struct Coordinate glasgow;
	glasgow.x = -4.2105;
	glasgow.y = 55.8627;
	struct Coordinate vilnius;
	vilnius.x = 25.2801;
	vilnius.y = 54.6858;
	struct Coordinate hongkong;
	hongkong.x = 114.0728;
	hongkong.y = 22.5310;
	struct Coordinate nairobi;
	nairobi.x = 36.8662;
	nairobi.y = -1.2985;
	
	ck_assert(is_inside_or_border(dublin.x, dublin.y, polygon, length, get_x, get_y));
	ck_assert(is_inside_or_border(monaghan.x, monaghan.y, polygon, length, get_x, get_y));
	ck_assert(is_inside_or_border(manorhamilton.x, manorhamilton.y, polygon, length, get_x, get_y));
	ck_assert(is_inside_or_border(bordernearmagheraaveli.x, bordernearmagheraaveli.y, polygon, length, get_x, get_y));
	ck_assert(is_inside_or_border(moville.x, moville.y, polygon, length, get_x, get_y));
	ck_assert(is_inside_or_border(backlion.x, backlion.y, polygon, length, get_x, get_y));
	ck_assert(is_inside_or_border(cork.x, cork.y, polygon, length, get_x, get_y));
	ck_assert(is_inside_or_border(galway.x, galway.y, polygon, length, get_x, get_y));
	ck_assert(is_inside_or_border(castletownbere.x, castletownbere.y, polygon, length, get_x, get_y));
	
	ck_assert(!is_inside_or_border(belfast.x, belfast.y, polygon, length, get_x, get_y));
	ck_assert(!is_inside_or_border(armagh.x, armagh.y, polygon, length, get_x, get_y));
	ck_assert(!is_inside_or_border(kilgarrowlaugh.x, kilgarrowlaugh.y, polygon, length, get_x, get_y));
	ck_assert(!is_inside_or_border(london.x, london.y, polygon, length, get_x, get_y));
	ck_assert(!is_inside_or_border(derry.x, derry.y, polygon, length, get_x, get_y));
	ck_assert(!is_inside_or_border(overatlanticcoast.x, overatlanticcoast.y, polygon, length, get_x, get_y));
	ck_assert(!is_inside_or_border(montreal.x, montreal.y, polygon, length, get_x, get_y));
	ck_assert(!is_inside_or_border(buenosaires.x, buenosaires.y, polygon, length, get_x, get_y));
	ck_assert(!is_inside_or_border(glasgow.x, glasgow.y, polygon, length, get_x, get_y));
	ck_assert(!is_inside_or_border(vilnius.x, vilnius.y, polygon, length, get_x, get_y));
	ck_assert(!is_inside_or_border(hongkong.x, hongkong.y, polygon, length, get_x, get_y));
	ck_assert(!is_inside_or_border(nairobi.x, nairobi.y, polygon, length, get_x, get_y));  
}
END_TEST


Suite * point_polygon_suite(void)
{
  Suite *s;
  TCase *tc;
  s = suite_create("Point inside polygon Suite");
  tc = tcase_create("Point inside polygon test case");
  tcase_add_test(tc, quadrant_test);
  tcase_add_test(tc, abscissa_projection_test);
  tcase_add_test(tc, calculate_increment_test);
  tcase_add_test(tc, is_inside_trapezoid_test);
  tcase_add_test(tc, is_inside_spiralid_test);
  tcase_add_test(tc, is_inside_ireland_test);
  suite_add_tcase(s, tc);
  return s;
}

int main(void)
{
  int number_failed;
  Suite *s;
  SRunner *sr;
  
  s = point_polygon_suite();
  sr = srunner_create(s);
  
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
