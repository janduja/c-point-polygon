CC=gcc
CFLAGS=`pkg-config --cflags --libs check glib-2.0 json-glib-1.0`

test: point-polygon-test
	./point-polygon-test

point-polygon-test: point-polygon-test.o point-polygon.o
	$(CC) -Wall point-polygon-test.o point-polygon.o -o point-polygon-test $(CFLAGS)

point-polygon-test.o: test/point-polygon-test.c
	$(CC) -Wall $(CFLAGS) -g -c test/point-polygon-test.c

point-polygon.o: point-polygon.c point-polygon.h
	$(CC) -Wall $(CFLAGS) -g -c point-polygon.c

clean:
	rm -f *.o point-polygon-test
