/* 
 * Copyright 2017 Raffaele Arecchi <raffaele.arecchi@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "point-polygon.h"

double abscissa_projection(
	double p_y,
	double v1_x,
	double v1_y,
	double v2_x,
	double v2_y)
{
	return v1_x + 
	   (((v2_x - v1_x )*( p_y - v1_y )) 
	    /(v2_y - v1_y));
}

int quadrant(double x1, double y1, double x2, double y2)
{
  if(x2 > x1 && y2 >= y1){
    return 0;
  } else if (x1 >= x2 && y2 > y1){
    return 1;
  } else if (x1 > x2 && y1 >= y2){
    return 2;
  } else if (x1 <= x2 && y1 > y2){
    return 3;
  } else {
    return -1;
  }
}

int calculate_increment(
	double p_x,
	double p_y,
	double last_x,
	double last_y,
	double next_x,
	double next_y)
{
	int last_quadrant = quadrant(p_x, p_y, last_x, last_y);
	int next_quadrant = quadrant(p_x, p_y, next_x, next_y);
	if ((p_x == last_x && p_y == last_y) || (p_x == next_x && p_y == next_y)) {
		return 9;
	} else if (last_quadrant == next_quadrant) {
		return 0;
	} else if (last_quadrant == 0 && next_quadrant == 1) {
		return 1;
	} else if (last_quadrant == 1 && next_quadrant == 2) {
		return 1;
	} else if (last_quadrant == 2 && next_quadrant == 3) {
		return 1;
	} else if (last_quadrant == 3 && next_quadrant == 0) {
		return 1;
	} else if (last_quadrant == 3 && next_quadrant == 2) {
		return -1;
	} else if (last_quadrant == 2 && next_quadrant == 1) {
		return -1;
	} else if (last_quadrant == 1 && next_quadrant == 0) {
		return -1;
	} else if (last_quadrant == 0 && next_quadrant == 3) {
		return -1;
	} else if (last_y == next_y) {
		return 9;
	}
	double proj = abscissa_projection(p_y, last_x, last_y, next_x, next_y);
	int proj_before = proj < p_x;
	if (p_x == proj) {
		return 9;
	} else if (proj_before && last_quadrant == 0) {
		return 2;
	} else if (proj_before && last_quadrant == 1) {
		return 2;
	} else if (proj_before && last_quadrant == 2) {
		return -2;
	} else if (proj_before && last_quadrant == 3) {
		return -2;
	} else if (!proj_before && last_quadrant == 0) {
		return -2;
	} else if (!proj_before && last_quadrant == 1) {
		return -2;
	} else if (!proj_before && last_quadrant == 2) {
		return 2;
	} else {
		return 2;
	}	
}

static int is_inside_or_border_rec (
 double p_x,
 double p_y,
 void **polygon,
 size_t nitems,
 double (*xcoordinate)(void*),
 double (*ycoordinate)(void*),
 int quadrant_increment,
 double last_x,
 double last_y,
 int initial
)
{	
	if (nitems == 0) {
		return quadrant_increment != 0;
	}
	double px_of_polyg = xcoordinate(*polygon);
	double py_of_polyg = ycoordinate(*polygon);
	int next_increment;
	if(initial) {
		next_increment = 0;
	} else{
		next_increment = calculate_increment(p_x, p_y, last_x, last_y, px_of_polyg, py_of_polyg);
	}
	if(next_increment == 9){
		return 1;
	}
	++polygon;
	return is_inside_or_border_rec(
		p_x,
		p_y,
		polygon,
		nitems - 1,
		xcoordinate,
		ycoordinate,
		quadrant_increment + next_increment,
		px_of_polyg,
		py_of_polyg,
		0
		);
}

int is_inside_or_border(
	double p_x,
	double p_y,
  void **polygon,
  size_t nitems,
  double (*xcoordinate)(void*),
  double (*ycoordinate)(void*)  
)
{
	return is_inside_or_border_rec(
		p_x,
		p_y,
		polygon,
		nitems,
		xcoordinate,
		ycoordinate,
		0,
		0,
		0,
		1
		);
}
